FROM node:14.18.2-alpine

WORKDIR /app

RUN apk update && apk add bash

#Depends
COPY ./package.json ./
COPY ./yarn.lock ./

RUN yarn install --frozen-lockfile

#BUILD
COPY ./public ./public/
COPY ./src ./src/
COPY tsconfig.json ./

RUN yarn build-ci

#Команда для запуска сервера внутри контейнера
CMD [ "yarn", "global-stand" ]
