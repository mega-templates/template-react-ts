import { observer } from "mobx-react-lite";
import { useEffect } from "react";
import ExampleListScreenVM from "./vm";

interface Props {
  vm: ExampleListScreenVM;
}

const View = ({ vm }: Props) => {
  useEffect(() => {
    vm.initDataList();
  }, []);

  return (
    <div>
      {vm.postList.map((item) => {
        return (
          <div>
            <div>{item.id}</div>
            <div>{item.userId}</div>
            <div>{item.title}</div>
            <div>{item.body}</div>
          </div>
        );
      })}
    </div>
  );
};

export const ExampleScreen = observer(View);
