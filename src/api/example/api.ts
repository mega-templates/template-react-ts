import { API } from "api";
import { ExamplePost } from "./classes";

const PATHS = {
  POSTS: "/posts",
};

export const ExampleAPI = {
  getAllPosts: async () => {
    return await API.get<ExamplePost[]>({
      url: PATHS.POSTS,
    });
  },
};
