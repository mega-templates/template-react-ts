import LogoImg from "assets/logo.png";
import { NavLink } from "react-router-dom";
import styles from "./styless.module.scss";

export const Logo = () => {
  return (
    <NavLink to={"/"} className={styles.logo}>
      <div className={styles.logo}>
        <img src={LogoImg} alt={"logo"} />
      </div>
    </NavLink>
  );
};
