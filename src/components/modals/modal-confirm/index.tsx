import { ExclamationCircleOutlined } from "@ant-design/icons";
import { Modal } from "antd";

interface Props {
  onOk?: () => void;
  onCancel?: () => void;
}

export const modalConfirm = (props: Props) => {
  Modal.confirm({
    title: "Вы точно хотите соверишть данное действие?",
    icon: <ExclamationCircleOutlined />,
    content: "",
    okText: "Да",
    cancelText: "Нет",
    onOk() {
      if (typeof props.onOk === "function") {
        props.onOk();
      }
    },
    onCancel() {
      if (typeof props.onCancel === "function") {
        props.onCancel();
      }
    },
  });
};
