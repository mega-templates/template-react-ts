import { EditOutlined } from "@ant-design/icons";
import { Button } from "antd";
import React from "react";

type Props = {
  onClick: (e: any) => void;
};

export const EditButton = (props: Props) => {
  return <Button type="primary" icon={<EditOutlined />} onClick={props.onClick} />;
};
