import styles from "./styles.module.scss";

export const BaseLoader = () => {
  return (
    <div className={styles.container}>
      <h2>Пожалуйста, подождите...</h2>
    </div>
  );
};