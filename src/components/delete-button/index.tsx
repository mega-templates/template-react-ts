import { DeleteOutlined } from '@ant-design/icons';
import { Button } from 'antd';
import React from 'react';

type Props = {
  onClick: (e: any) => void;
};

export const DeleteButton = (props: Props) => {
  return (
    <Button
      type="primary"
      icon={<DeleteOutlined />}
      onClick={props.onClick}
      style={{
        background: '#FF6347',
        border: 'none',
      }}
    />
  );
};
