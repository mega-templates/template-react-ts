import { Layout, Menu, MenuProps } from "antd";
import { ItemType } from "antd/lib/menu/hooks/useItems";
import MenuItem from "antd/lib/menu/MenuItem";
import { observer } from "mobx-react-lite";
import React from "react";
import { useNavigate } from "react-router-dom";
import { ROUTES } from "router";
import styles from "./styles.module.scss";

export const NavBlock = observer(() => {
  const navigate = useNavigate();

  const toPage = (url: string) => {
    navigate(url);
  };

  function getItem(key: React.Key, label: React.ReactNode): ItemType {
    return {
      key,
      label,
    } as ItemType;
  }

  const items: ItemType[] = [
    getItem("Option 1", "1"),
    getItem("Option 2", "2"),
    getItem("Files", "9"),
  ];

  const onClick: MenuProps["onClick"] = (e) => {
    console.log("click ", e);
  };

  return (
    <Layout.Sider>
      <Menu
        onClick={onClick}
        theme="light"
        mode="inline"
        className={styles.menu}
        // items={items}
      >
        {/* <Menu.Item key="1" onClick={() => toPage(ROUTES.SA.COURSES.path)}>
          Курсы
        </Menu.Item>
        <Menu.Item key="3" onClick={() => toPage(ROUTES.SA.MARKS.path)}>
          Оценки
        </Menu.Item>
        <Menu.Item key="4" onClick={() => toPage(ROUTES.SA.GROUPS.path)}>
          Группы
        </Menu.Item>
        <Menu.Item key="2" onClick={() => toPage(ROUTES.SA.STUDENTS.path)}>
          Студенты
        </Menu.Item>
        <Menu.Item key="5" onClick={() => toPage(ROUTES.SA.USERS.path)}>
          Пользователи
        </Menu.Item> */}
      </Menu>
    </Layout.Sider>
  );
});
