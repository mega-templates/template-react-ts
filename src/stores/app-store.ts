import { API } from "api";
import { toastError } from "components/toasts/toast-error";
import { makeAutoObservable } from "mobx";

class AppStore {
  isInit: boolean = false;

  constructor() {
    makeAutoObservable(this);
  }

  initApp() {
    API.setOnRequestError(async (error: any) => {
      toastError("Some error");
      return false;
    });

    this.isInit = true;
  }
}

export default new AppStore();
