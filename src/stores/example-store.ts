import { ExampleAPI } from 'api/example/api';
import { ExamplePost } from 'api/example/classes';
import { makeAutoObservable } from 'mobx';

class ExampleStore {
  postList: ExamplePost[] = [];

  constructor() {
    makeAutoObservable(this);
  }

  async initDataList() {
    const resp = await ExampleAPI.getAllPosts();

    if (resp.isSuccess) {
      if (resp.data) {
        this.postList = resp.data;
      }
    }
  }
}

export default new ExampleStore();
