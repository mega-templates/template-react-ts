export const ROUTES = {
  ROOT: {
    path: '/',
    createPath: (params?: URLSearchParams) => ROUTES.ROOT.path + '?' + (params?.toString() ?? '')
  },
  NOT_FOUND: {
    path: '/404',
  },
  SERVER_ERROR: {
    path: '/500',
  },
  ABOUT: {
    path: '/about',
  },
  SA: {
    path: '/sa'
  }
}

//Example - для получения параметров из path PARAMS. students/:ID/action
// GIFTS: {
//   path: '/gifts',
//   PAYMENT_RESULT: {
//     path: '/gifts/:giftId/payment',
//     createPath: (giftId: string) => `/gifts/${ giftId }/payment`,
//   },
// },