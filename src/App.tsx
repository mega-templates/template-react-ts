/* mobx */
import { observer } from "mobx-react-lite";
import { ExampleScreen } from "pages/example";
import ExampleListScreenVM from "pages/example/vm";
/* PAGES */
import { BaseLoader } from "components/base-loader";
import { useEffect } from "react";
/* components */
import { ToastContainer } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import appStore from "stores/app-store";
import "./App.scss";

const App = observer(() => {
  useEffect(() => {
    //INIT APP!
    appStore.initApp();
  }, []);

  return (
    <div className="App">
      {appStore.isInit ? <ExampleScreen vm={new ExampleListScreenVM()} /> : <BaseLoader />}

      <ToastContainer position="bottom-center" autoClose={3000} hideProgressBar={false} />
    </div>
  );
});

export default App;
